﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace ForkBets
{
    class BetGenerator : INotifyPropertyChanged
    {
        private const int BROKERS_COUNT = 10;
        private static BetGenerator instance = new BetGenerator();
        private ObservableCollection<Bet> bets = new ObservableCollection<Bet>();
        private ObservableCollection<Bet> doneBets = new ObservableCollection<Bet>();
        private List<Broker> brokers = new List<Broker>();
        private Random random = new Random();
        private RealEventExecutor eventExecutor = RealEventExecutor.getExecutor();

        public event PropertyChangedEventHandler PropertyChanged;

        private BetGenerator()
        {
            for (int i = 0; i < BROKERS_COUNT; i++)
            {
                brokers.Add(BrokerFactory.getBroker());
            }

            eventExecutor.getEvents().CollectionChanged += (sender, e) =>
            {
                if(e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
                {
                    foreach (RealEvent evt in e.NewItems)
                    {
                        generate(evt);
                    }
                }
            };
        }

        public ObservableCollection<Bet> getBets()
        {
            return bets;
        }

        public ObservableCollection<Bet> getDoneBets()
        {
            return doneBets;
        }

        public static BetGenerator getGenerator()
        {
            return instance;
        }

        public void generate(RealEvent evt)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            foreach (var broker in brokers)
            {
                if(random.NextDouble() > 0.5)
                {
                    Bet bet = broker.provideBet(evt);
                    bet.PropertyChanged += async (s, e) =>
                    {
                        if(e.PropertyName == "BetValue")
                        {
                            await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                            {
                                bets.Remove(bet);
                            });

                            if(bet.Made)
                            {
                                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                                {
                                    if(!doneBets.Contains(bet))
                                        doneBets.Insert(0, bet);
                                });
                            }
                        }
                    };

                    bets.Add(bet);
                }
            }
            OnPropertyChanged("Generated");
        }

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
