﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class ForkBet : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private RealEvent evt;
        private bool c1, c2, draw;
        private Bet bet1, bet2;
        private Decimal amount = 0;

        public string Name
        {
            get { return evt.Title; }
        }

        public string Broker1
        {
            get { return bet1.BrokerName; }
        }

        public string Broker2
        {
            get { return bet2.BrokerName; }
        }

        public Decimal C1
        {
            get { return (c1) ? bet1.Win1 : bet2.Win1; }
        }

        public Decimal C2
        {
            get { return (c2) ? bet1.Win2 : bet2.Win2; }
        }

        public Decimal WinAmount
        {
            get { return Math.Round(1 - (1 / C1 + 1 / C2), 2); }
        }    

        public bool Made
        {
            get { return amount == 0; }
        }    

        public Decimal Amount
        {
            get { return amount; }
        }

        public bool BetAvaliable
        {
            get { return !bet1.Made && !bet2.Made; }
        }

        public int TimeLeft
        {
            get { return evt.TimeLeft; }
        }

        public ForkBet(Bet bet1, Bet bet2, bool c1, bool c2)
        {
            this.bet1 = bet1;
            this.bet2 = bet2;
            this.evt = bet1.getEvent();
            this.c1 = c1;
            this.c2 = c2;
            evt.PropertyChanged += (s, e) => {
                OnPropertyChanged(e.PropertyName);
            };
        }

        public void makeBet(Decimal value)
        {
            if (!BetAvaliable)
                return;
            var b = (c1) ? bet1 : bet2;
            b.setBet(EventResult.First, C2 / (C1 + C2) * value);

            b = (c2) ? bet1 : bet2;
            b.setBet(EventResult.Second, C1 / (C1 + C2) * value);

            amount = value;

            OnPropertyChanged("BetAvaliable");
        }

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
