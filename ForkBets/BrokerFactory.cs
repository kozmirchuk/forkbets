﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class BrokerFactory
    {
        private static List<String> firstWord = new List<String>()
        { "Sky", "Club", "Best", "", "Bet", "Football", "Sport",
        "Top", "Play", "Home"};

        private static List<String> secondWord = new List<String>()
        { "Bet", "365", "10", "Betting", "Bwin", "Win", "Luck" };

        private static Random random = new Random();

        private BrokerFactory() { }

        public static Broker getBroker()
        {
            Broker broker = new Broker(firstWord[random.Next(firstWord.Count)] + " " +
                secondWord[random.Next(secondWord.Count)]);
            return broker;
        }
    }
}
