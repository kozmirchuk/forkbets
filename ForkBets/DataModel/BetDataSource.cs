﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Windows.Data.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The data model defined by this file serves as a representative example of a strongly-typed
// model.  The property names chosen coincide with data bindings in the standard item templates.
//
// Applications may use this model as a starting point and build on it, or discard it entirely and
// replace it with something appropriate to their needs. If using this model, you might improve app 
// responsiveness by initiating the data loading task in the code behind for App.xaml when the app 
// is first launched.

namespace ForkBets.Data
{
    /// <summary>
    /// Generic item data model.
    /// </summary>
    public class BetDataItem
    {
        public BetDataItem(String guid, String name, double left, double draw, double right, String brokerGuid, int timeLeft)
        {
            this.GUID = guid;
            this.Name = name;
            this.Left = left;
            this.Right = right;
            this.Draw = draw;
            this.BrokerGUID = brokerGuid;
            this.TimeLeft = timeLeft;
        }

        public string GUID { get; private set; }
        public string Name { get; private set; }
        public double Left { get; private set; }
        public double Right { get; private set; }
        public double Draw { get; private set; }
        public string BrokerGUID { get; private set; }
        public int TimeLeft { get; private set; }

        public override string ToString()
        {
            return this.Name;
        }
    }

    /// <summary>
    /// Generic group data model.
    /// </summary>
    public class BetDataGroup
    {
        public BetDataGroup(String uniqueId, String title, String subtitle, String imagePath, String description)
        {
            this.UniqueId = uniqueId;
            this.Title = title;
            this.Subtitle = subtitle;
            this.Description = description;
            this.ImagePath = imagePath;
            this.Items = new ObservableCollection<BetDataItem>();
        }

        public string UniqueId { get; private set; }
        public string Title { get; private set; }
        public string Subtitle { get; private set; }
        public string Description { get; private set; }
        public string ImagePath { get; private set; }
        public ObservableCollection<BetDataItem> Items { get; private set; }

        public override string ToString()
        {
            return this.Title;
        }
    }

    /// <summary>
    /// Creates a collection of groups and items with content read from a static json file.
    /// 
    /// SampleDataSource initializes with data read from a static json file included in the 
    /// project.  This provides sample data at both design-time and run-time.
    /// </summary>
    public sealed class BetDataSource
    {
        private static BetDataSource _sampleDataSource = new BetDataSource();

        private ObservableCollection<BetDataGroup> _groups = new ObservableCollection<BetDataGroup>();
        public ObservableCollection<BetDataGroup> Groups
        {
            get { return this._groups; }
        }

        public static async Task<IEnumerable<BetDataGroup>> GetGroupsAsync()
        {
            await _sampleDataSource.GetSampleDataAsync();

            return _sampleDataSource.Groups;
        }

        public static async Task<BetDataGroup> GetGroupAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.Where((group) => group.UniqueId.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        public static async Task<BetDataItem> GetItemAsync(string uniqueId)
        {
            await _sampleDataSource.GetSampleDataAsync();
            // Simple linear search is acceptable for small data sets
            var matches = _sampleDataSource.Groups.SelectMany(group => group.Items).Where((item) => item.GUID.Equals(uniqueId));
            if (matches.Count() == 1) return matches.First();
            return null;
        }

        private async Task GetSampleDataAsync()
        {
            if (this._groups.Count != 0)
                return;

            Uri dataUri = new Uri("ms-appx:///DataModel/BetData.json");

            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(dataUri);
            string jsonText = await FileIO.ReadTextAsync(file);
            JsonObject jsonObject = JsonObject.Parse(jsonText);
            JsonArray jsonArray = jsonObject["Groups"].GetArray();

            foreach (JsonValue groupValue in jsonArray)
            {
                JsonObject groupObject = groupValue.GetObject();
                BetDataGroup group = new BetDataGroup(groupObject["GUID"].GetString(),
                                                      groupObject["Title"].GetString(),
                                                      groupObject["Subtitle"].GetString(),
                                                      groupObject["ImagePath"].GetString(),
                                                      groupObject["Description"].GetString());

                foreach (JsonValue itemValue in groupObject["Items"].GetArray())
                {
                    JsonObject itemObject = itemValue.GetObject();
                    group.Items.Add(new BetDataItem(itemObject["GUID"].GetString(),
                                                    itemObject["Name"].GetString(),
                                                    itemObject["Left"].GetNumber(),
                                                    itemObject["Right"].GetNumber(),
                                                    itemObject["Draw"].GetNumber(),
                                                    itemObject["BrokerGUID"].GetString(),
                                                    (int) itemObject["TimeLeft"].GetNumber()
                                                    ));
                }
                this.Groups.Add(group);
            }
        }
    }
}