﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class RealEvent : INotifyPropertyChanged
    {
        private string guid = System.Guid.NewGuid().ToString();
        public string GUID
        {
            get { return guid; }
        }
        private String name;
        private double p1, p2;
        private int time;
        private EventResult result = EventResult.None;

        public event PropertyChangedEventHandler PropertyChanged;

        public String Title
        {
            get { return name; }
        }

        public String Description
        {
            get {
                return "Expires in: " + time + " sec.\nTeam 1: " + Math.Round(p1 * 100) + "%, Team 2: " + Math.Round(p2 * 100) + "%";
            }
        }

        public EventResult Result
        {
            get { return result; }
        }

        public double P1
        {
            get { return p1; }
        }

        public double P2
        {
            get { return p2; }
        }

        public int TimeLeft
        {
            get { return time; }
        }

        public bool HasHappened()
        {
            return time <= 0;
        }

        public RealEvent(String name, double c1, double c2, int time)
        {
            this.name = name;
            this.p1 = c1;
            this.p2 = c2;
            this.time = time;
        }
       
        public void passTime(int delta)
        {
            time -= delta;
            OnPropertyChanged("Description");
            OnPropertyChanged("TimeLeft");
            if (time <= 0)
            {
                time = 0;
                double r = new Random().NextDouble();
                result = (r <= p1)? EventResult.First : EventResult.Second;
                OnPropertyChanged("Result");
            }
        }

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }

    class RealEventGenerator
    {
        private static Random r = new Random();

        public static IEnumerable<RealEvent> Generate(int n)
        {
            IList<RealEvent> result = new List<RealEvent>();

            for(int i = 0; i < n; i++)
            {
                double c1 = r.NextDouble();
                double c2 = 1 - c1;

                int time = 5 + r.Next(120 - 5);  // 5 - 120 seconds

                yield return new RealEvent(genName(), c1, c2, time);
            }
        }

        private static String genName()
        {
            String[] prefix = { "Super", "Mega", "Universal", "World", "Planetary" };
            String[] type = { "beer", "horse", "pizza", "potato", "pig" };
            String[] action = { "riding", "consuming", "rolling", "shooting", "throwing" };
            String[] evt = { "championship", "competition", "bowl", "olympics" };
            return 
                prefix[r.Next(prefix.Length)] + " " +
                type[r.Next(type.Length)] + " " +
                action[r.Next(action.Length)] + " " +
                evt[r.Next(evt.Length)];
        }        
    }

    enum EventResult
    {
        None,
        First,
        Second
    }
}
