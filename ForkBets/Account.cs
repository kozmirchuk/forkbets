﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class Account
    {
        private static Account instance = new Account();

        private const String ACC_VAL = "accountValue";

        private Decimal value = 0;

        public Decimal Value
        {
            get { return value; }
        }

        private Account()
        {
            loadValue();
        }

        private void loadValue()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey(ACC_VAL))
            {
                String v = (String)localSettings.Values[ACC_VAL];
                value = Decimal.Parse(v);
            } else
            {
                value = 100;
            }     
        }

        public void saveValue()
        {
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values[ACC_VAL] = value.ToString();
        }

        public static Account getAccount()
        {
            return instance;
        }

        public void add(decimal x)
        {
            value = Math.Round(value + x, 2);
        }

        public void sub(decimal x)
        {
            value = Math.Round(value - x, 2);
        }
    }
}
