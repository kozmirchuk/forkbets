﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace ForkBets
{
    class ForkBetsFinder
    {
        private static ForkBetsFinder instance = new ForkBetsFinder();
        private ObservableCollection<ForkBet> forkBets = new ObservableCollection<ForkBet>();
        private HashSet<Bet> used = new HashSet<Bet>();

        private ForkBetsFinder()
        {
            BetGenerator.getGenerator().getBets().CollectionChanged += ForkBetsFinder_CollectionChanged;
        }

        private void ForkBetsFinder_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add)
            {
                foreach (Bet bet in e.NewItems)
                {
                    generate(bet.EventGUID);
                }
            }
        }

        public void generate(string guid)
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            Bet[] bets = BetGenerator.getGenerator().getBets().Where(b => b.EventGUID == guid && !used.Contains(b)).ToArray();
            for (int i = 0; i < bets.Length; i++)
            {
                if (bets[i].Made)
                    continue;

                for (int j = i + 1; j < bets.Length; j++)
                {
                    if (bets[j].Made)
                        continue;
                                      
                    var c1 = (bets[i].Win1 > bets[j].Win1)? bets[i].Win1 : bets[j].Win1;
                    var r1 = (bets[i].Win1 > bets[j].Win1);

                    var c2 = (bets[i].Win2 > bets[j].Win2) ? bets[i].Win2 : bets[j].Win2;
                    var r2 = (bets[i].Win2 > bets[j].Win2);

                    if(1/c1 + 1/c2 < 0.99m)
                    {
                        var fb = new ForkBet(bets[i], bets[j], r1, r2);
                        fb.PropertyChanged += async (e, s) =>
                        {
                            if (!fb.BetAvaliable || fb.TimeLeft <= 0)
                            {
                                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                                {
                                    forkBets.Remove(fb);
                                });
                            }
                        };
                        forkBets.Add(fb);
                        used.Add(bets[i]);
                        used.Add(bets[j]);
                    }
                }
            }            
        }

        public static ForkBetsFinder getFinder()
        {
            return instance;
        }

        public ObservableCollection<ForkBet> getForkBets()
        {
            return forkBets;            
        }
    }
}
