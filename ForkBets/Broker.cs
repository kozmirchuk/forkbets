﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class Broker
    {
        private Random random = new Random();
        public String Name
        {
            get;
        }
        private List<Bet> bets = new List<Bet>();

        public Broker(String name)
        {
            this.Name = name;
        }

        public Bet provideBet(RealEvent evt)
        {
            double drift = random.NextDouble() * 0.15 - 0.075;   // 15%
            Decimal c1 = (decimal) (1 / Math.Max(evt.P1 + drift, 0.00001));
            Decimal c2 = (decimal) (1 / Math.Max(evt.P2 - drift, 0.00001));
            Bet bet = new Bet(this, evt, c1, c2);
            bets.Add(bet);
            return bet;
        }

        public List<Bet> getBets()
        {
            return bets;
        }
    }
}
