﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForkBets
{
    class Bet : INotifyPropertyChanged
    {
        private Broker broker;
        private RealEvent evt;
        private bool paid = false;
        public event PropertyChangedEventHandler PropertyChanged;

        public string GUID
        {
            get; private set;
        }
        public string EventGUID
        {
            get { return evt.GUID; }
        }
        public Decimal Win1
        {
            get;
            private set;
        }
        public Decimal Win2
        {
            get;
            private set;
        }
        public string Title
        {
            get
            {
                return evt.Title;
            }
        }
        public string BrokerName
        {
            get { return broker.Name; }
        }
        public string Description
        {
            get
            {
                return
                    "Broker: " + broker.Name +
                    "\nCoeff: " + Win1 + ":" + Win2 +
                    "\nExpires in: " + evt.TimeLeft;                                                          
            }
        }
        public bool Made
        {
            get { return Prediction != EventResult.None; }
        }
        public EventResult Prediction
        {
            get; private set;
        }
        public Decimal BetValue
        {
            get; private set;
        }
        public Decimal WinValue
        {
            get
            {
                if (evt.Result == EventResult.None)
                    return 0;
                return (Prediction == evt.Result) ? BetValue * ((Prediction == EventResult.First) ? Win1 : Win2) : 0;
            }
        }
        public string SPrediction
        {
            get {
                if (!Made)
                    return "No prediction";
                return (Prediction == EventResult.First)? "Team 1" : "Team 2";
            }
        }
        public string Winner
        {
            get
            {
                if (evt.Result == EventResult.None)
                    return "Nobody";
                return (evt.Result == EventResult.First) ? "Team 1" : "Team 2";                
            }
        }
        public int TimeLeft
        {
            get { return evt.TimeLeft; }
        }

        public string Result
        {
            get {
                if (TimeLeft > 0)
                    return "Result uknown";
                else return "You " + ((Prediction == evt.Result) ? "win!" : "lose!");
            }
        }

        public Bet(Broker broker, RealEvent evt, Decimal win1, Decimal win2)
        {
            this.broker = broker;
            this.evt = evt;
            this.GUID = System.Guid.NewGuid().ToString();
            this.Win1 = win1;
            this.Win2 = win2;
            this.BetValue = 0;
            this.Prediction = EventResult.None;
            evt.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "Result")
                {
                    if (evt.Result == Prediction)
                        Account.getAccount().add(WinValue);
                    OnPropertyChanged("WinValue");
                    OnPropertyChanged("Winner");
                }
                OnPropertyChanged(args.PropertyName);                
            };
        }

        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public void setBet(EventResult result, Decimal value)
        {
            if(!Made && result != EventResult.None)
            {
                this.BetValue = value;
                this.Prediction = result;
                Account.getAccount().sub(value);
                OnPropertyChanged("BetValue");
            }
        }

        public RealEvent getEvent()
        {
            return evt;
        }
    }
}
