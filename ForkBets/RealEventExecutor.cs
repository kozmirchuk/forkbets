﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Windows.UI.Core;

namespace ForkBets
{
    class RealEventExecutor
    {       
        private static bool stop = false;
        private static ObservableCollection<RealEvent> events = new ObservableCollection<RealEvent>();
        private static RealEventExecutor instance = new RealEventExecutor();

        private RealEventExecutor()
        {
            tick();
        }
        
        public ObservableCollection<RealEvent> getEvents()
        {
            return events;
        }

        public static RealEventExecutor getExecutor()
        {
            return instance;
        }

        private async void tick()
        {
            CoreDispatcher dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
            Stopwatch sw = new Stopwatch();
            List<RealEvent> toBeRemoved = new List<RealEvent>();
            while (!stop)
            {
                sw.Reset();
                sw.Start();
                foreach (RealEvent evt in events)
                {
                    evt.passTime(1);
                    if (!evt.HasHappened())
                        continue;

                    toBeRemoved.Add(evt);
                }

                //Remove all events, which are already happened

                await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {                
                    foreach (RealEvent e in toBeRemoved)
                    {
                        events.Remove(e);
                    }
                });
                                
                toBeRemoved.Clear();

                if (events.Count < 10)
                {
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        foreach (RealEvent n in RealEventGenerator.Generate(10))
                            events.Add(n);
                    });
                }

                sw.Stop();
                await Task.Delay(TimeSpan.FromMilliseconds(Math.Max(1000 - sw.ElapsedMilliseconds, 0)));
            }
        }
    }
}